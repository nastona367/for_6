from django import forms
from django.contrib import admin
from .models import *


class ProductAdmin(admin.ModelAdmin):
    list_display = ('category', 'product', 'price', 'year')


class CategoryAdmin(admin.ModelAdmin):
    list_displ = 'name'


admin.site.register(Products, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
