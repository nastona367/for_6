from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=20, verbose_name="Назва категорії", blank=True)


class Products(models.Model):
    category = models.ForeignKey('Category', on_delete=models.CASCADE,
                                 null=True, verbose_name='Категория')
    product = models.CharField(max_length=20, verbose_name="Назва продукту", blank=True)
    price = models.IntegerField(verbose_name="Ціна", blank=True)
    year = models.DateField(verbose_name="Дата виготовлення", blank=True)


