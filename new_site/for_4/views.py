from django.shortcuts import render
from .models import *
from django.db.models import Q

def main(request):
    all_words = Products.objects.all()
    l_words = Products.objects.filter(Q(product__startswith='Л'))
    return render(request, 'pages/for_4.html', {'one': all_words, 'two': l_words})
