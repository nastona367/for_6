from django.urls import path
from .views import *

urlpatterns = [
    path('', main, name='main'),
    path('new_web/', new_web, name='new_web')]
