from django.shortcuts import render
from .models import *
from django.http import HttpResponse

def new_web(request):
    if request.method == 'GET':
        if 'something' in request.GET:
            if request.GET['something'] == '':
                return HttpResponse('Ви нічого не ввели!')
            else:
                return HttpResponse("Ви ввели %s" % request.GET['something'])
        else:
            return HttpResponse('Помилка!')


def main(request):
    return render(request, 'pages/for_3.html')
