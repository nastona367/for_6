from django.apps import AppConfig


class For3Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'for_3'
