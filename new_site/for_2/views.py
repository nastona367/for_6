from django.shortcuts import render
from .models import *
from django.db.models import Q

def main(request):
    all_words = Course.objects.all()
    two_words = Course.objects.all()[:2]
    three_words = Course.objects.filter(
        Q(n__startswith="a"))
    four_words = Course.objects.filter(
        ~Q(n__startswith="a"))
    return render(request, 'pages/main.html', {'one': all_words,
                                               'two': two_words,
                                               'three': three_words,
                                               'four': four_words})
