from django.contrib import admin
from .models import *
from django import forms

class CourseAdmin(admin.ModelAdmin):
    list_display = ('n',)


admin.site.register(Course, CourseAdmin)
